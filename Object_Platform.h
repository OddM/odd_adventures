#ifndef OBJECT_PLATFORM_H
#define OBJECT_PLATFORM_H

#include "Object_IObject.h"
#include "raylib.h"
#include <cstdint>

namespace Object
{
   class Platform 
      : public Object::IObject
   {
      public:
         explicit Platform(float, float, Vector2);

         void render(float deltaTime) override;
         Rectangle getCollisionRec() const override;

      private:
         const float width_;
         const float height_;
         Vector2 position_;
         Rectangle rectangle_;

         Texture2D texture {LoadTexture("textures/12_nebula_spritesheet.png")};

   };
}



#endif