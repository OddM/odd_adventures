#include "Object_Actor.h"
#include "Core/Core_Constants.h"
#include <iostream>
#include <vector>
#include <memory>

Object::Actor::Actor(Texture2D texture, Vector2 position, std::int16_t maxFrames)
   : texture_(texture)
   , position_(position)
   , maxFrames_(maxFrames)
{
   viewPane_.width = texture.width / maxFrames;
   viewPane_.height = texture.height;
}


void
Object::Actor::render(float deltaTime)
{
   updateViewPane(deltaTime);
   movement(deltaTime);

   DrawTextureRec(texture_, viewPane_, position_, WHITE);
}


void
Object::Actor::jumping(float deltaTime)
{
   if(isGrounded)
   {
      velocity_ = 0.0F;
      isAirborn_ = false;
   }
   else 
   {
      velocity_ += Core::Constants::gravity * deltaTime;
      isAirborn_ = true;
   }

   if(IsKeyPressed(KEY_SPACE) && !isAirborn_)
   {
      velocity_ += -600.0F;
   }

   position_.y += velocity_ * deltaTime;
}

void
Object::Actor::movement(float deltaTime)
{
   jumping(deltaTime);
   float speedChange{};
   const float airbornMultiplier = isAirborn_ ? 0.5F : 1.0F;

   if(IsKeyDown(KEY_A) && speed_ > maxSpeed_*-1.0F)
   {
      if(speed_ > 0.0F && !isAirborn_)
      {
         speed_ *= 0.8F;
      }
      speedChange -= baseMovementSpeed_;
   }

   if(IsKeyDown(KEY_D) && speed_ < maxSpeed_)
   {
      if(speed_ < 0.0F && !isAirborn_)
      {
         speed_ *= 0.8F;
      }
      speedChange += baseMovementSpeed_;
   }

   if(!IsKeyDown(KEY_D) && !IsKeyDown(KEY_A) && !isAirborn_)
   {
      speed_ *= 0.8F;
      if(speed_ < 0.2F && speed_ > -0.2F)
      {
         speed_ = 0.0F;
      }
   }
   speed_ += speedChange * airbornMultiplier;
   std::cout << "Speed: " << speed_ << std::endl;
   
   position_.x += speed_ * deltaTime;
}


void
Object::Actor::updateViewPane(float deltaTime)
{
   runningTime_ += deltaTime;
   if(runningTime_ >= updateTime_)
   {
      runningTime_ = 0.0F;
      viewPane_.x = frame_ * viewPane_.width;
      frame_++;
      frame_ %= maxFrames_;
   }
}


Rectangle
Object::Actor::getCollisionRec() const
{
   return {position_.x, position_.y, viewPane_.width, viewPane_.height};
}


void
Object::Actor::updateCollision(const std::vector<std::unique_ptr<IObject>>& objects)
{
   bool checkGrounded {false};
   for (const auto& obj : objects)
   {
      if(CheckCollisionRecs(getCollisionRec(), obj->getCollisionRec()))
      {
         std::cout << "Objects Y: " << obj->getCollisionRec().y << "My Y + height: " << (position_.y + viewPane_.height) << std::endl;
         if(obj->getCollisionRec().y + 10 > (position_.y + viewPane_.height))
         {
            std::cout << "I am grounded." << std::endl;
            checkGrounded = true;
         }
      }
   }
   isGrounded = checkGrounded; 
}