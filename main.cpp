#include "raylib.h"
#include "Object_Actor.h"
#include "Object_Platform.h"
#include "Object_IObject.h"
#include "Core/Core_Constants.h"
#include <array>
#include <string>
#include <iostream>
#include <memory>

int main()
{
   InitWindow(Core::Constants::windowDimensions.at(0), Core::Constants::windowDimensions.at(1), Core::Constants::title.c_str());

   // Main Character
   Texture2D scarfyTexture = LoadTexture("textures/scarfy.png");
   Vector2 scarftPos {100,100};
   Object::Actor scarfy {scarfyTexture, scarftPos, 6};

   // Platform
   Object::Platform platform {100, 100, {250, 350}};
   Vector2 groundLevel = {0, static_cast<float>(Core::Constants::windowDimensions.at(1) - 50)};
   Object::Platform ground {250, 50, groundLevel};
   std::vector<std::unique_ptr<Object::IObject>> currentDisplayedObjects;
   currentDisplayedObjects.emplace_back(std::make_unique<Object::Platform>(platform));
   currentDisplayedObjects.emplace_back(std::make_unique<Object::Platform>(ground));



   SetTargetFPS(60);
   while(!WindowShouldClose())
   {
      const float deltaTime{GetFrameTime()};
      BeginDrawing();
      ClearBackground(BLUE);

      DrawText("Hello World!", 100, 100, 40, GREEN);

      scarfy.updateCollision(currentDisplayedObjects);
      scarfy.render(deltaTime);
      platform.render(deltaTime);
      ground.render(deltaTime);

      std::cout << "Collison " << CheckCollisionRecs(scarfy.getCollisionRec(), platform.getCollisionRec()) << std::endl;

      EndDrawing();
   }

   return 0;
}