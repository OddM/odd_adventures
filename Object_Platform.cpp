#include "Object_Platform.h"
#include "raylib.h"
#include <cstdint>

Object::Platform::Platform(float width, float height, Vector2 pos)
   : width_(width)
   , height_(height)
   , position_(pos)
{
   rectangle_ = {0, 0, width_, height_};
}


void
Object::Platform::render(float deltaTime)
{
   DrawTextureRec(texture, rectangle_, position_, GREEN);
}


Rectangle
Object::Platform::getCollisionRec() const
{
   return {position_.x, position_.y, width_, height_};
}