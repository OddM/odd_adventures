#ifndef OBJECT_IOBJECT_H
#define OBJECT_IOBJECT_H

#include "raylib.h"

namespace Object 
{
   class IObject 
   {
      public:
         virtual void render(float deltaTime) = 0;
         virtual Rectangle getCollisionRec() const = 0;
   };
}

#endif