#ifndef CORE_CONSTANTS_H
#define CORE_CONSTANTS_H

#include <array>
#include <cstdint>
#include <string>


namespace Core 
{
   namespace Constants 
   {
      const std::array<std::int16_t, 2> windowDimensions{800, 500};
      const std::string title {"Odd Adventures"};
      const std::int16_t gravity {1'000};
   }
}

#endif