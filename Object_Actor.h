#ifndef OBJECT_ACTOR_H
#define OBJECT_ACTOR_H

#include "Object_IObject.h"
#include "raylib.h"
#include <cstdint>
#include <vector>
#include <memory>

namespace Object 
{
   class Actor 
      : IObject
   {
      public:
         Actor(Texture2D texture, Vector2 position, std::int16_t maxFrames);
         void render(float deltaTime) override;
         Rectangle getCollisionRec() const override;
         void updateCollision(const std::vector<std::unique_ptr<IObject>>&);
         
      private:
         bool isOnGround();
         void updateViewPane(float deltaTime);
         void jumping(float deltaTime);
         void movement(float deltaTime);

         Texture2D texture_{};
         Rectangle viewPane_{};
         Vector2 position_{};
         std::int16_t frame_{};
         std::int16_t maxFrames_{};
         float updateTime_{1.0F / 12.0F};
         float runningTime_{};
         bool isAirborn_{};
         float velocity_{};
         float speed_ {};
         const float maxSpeed_ {400.0F};
         const float baseMovementSpeed_ {10.0F};
         bool isGrounded{false};
   };
}


#endif